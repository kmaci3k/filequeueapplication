﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonModule.MVVM;
using FileQueueModule.Models;
using System.Windows.Input;
using System.Windows;
using Microsoft.Practices.Prism.Commands;
using System.Collections.ObjectModel;
using FileQueueModule.DataModel;
using System.Windows.Media;

namespace FileQueueModule.ViewModels
{
    public class FileQueueViewModel : ExtendedViewModel<FileQueueModel>
    {
        public ObservableCollection<FileQueueItem> Items { get; set; }

        public FileQueueViewModel() : base(new FileQueueModel())
        {
            
        }

        #region Initialization
        
        public override void Initialize()
        {
            base.Initialize();
        }

        public override void InitializeModel()
        {
            base.InitializeModel();

            Items = new ObservableCollection<FileQueueItem>();

            List<StorageObject> storageObjects = new List<StorageObject>();
            storageObjects.Add(createStorageObject(@"D:/", Brushes.LightGreen));
            storageObjects.Add(createStorageObject(@"E:/", Brushes.LightPink));

            FileQueueItem item = new FileQueueItem()
            {
                SourceDirectory = @"C:/Dropbox/photo.png",
                StorageObjects = storageObjects,
                TargetDirectory = @"E:/Dysk Google/other_photo.png"
            };

            Items.Add(item);
            Items.Add(item);
        }

        private StorageObject createStorageObject(String targetDisk, Brush color)
        {
            StorageObject storageObject = new StorageObject()
            {
                TargetDisk = targetDisk,
                Color = color,
            };
            return storageObject;
        }

        public override void RegisterCommands()
        {
            base.RegisterCommands();

            Confirm = new DelegateCommand(ConfirmDelegate);
            CloseWindow = new DelegateCommand(CloseWindowDelegate);
        }

        #endregion

        #region Commands handlers

        public ICommand Confirm { get; private set; }
        private void ConfirmDelegate()
        {
            //Confirm and close
            Window mainWindow = Application.Current.MainWindow;
            mainWindow.Close();
        }

        public ICommand CloseWindow { get; private set; }
        private void CloseWindowDelegate()
        {
            Window mainWindow = Application.Current.MainWindow;
            mainWindow.Close();
        }

        #endregion
    }
}
