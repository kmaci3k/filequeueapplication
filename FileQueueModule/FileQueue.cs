﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Prism.Regions;
using FileQueueModule.ViewModels;
using FileQueueModule.Views;
using CommonModule.MVVM;

namespace FileQueueModule
{
    public class FileQueue : IModule
    {
        [Dependency]
        public IUnityContainer Container { get; set; }

        [Dependency]
        public IRegionManager RegionManager { get; set; }

        public void Initialize()
        {
            RegisterTypes();

            FileQueueViewModel vm = Container.Resolve<FileQueueViewModel>();
            FileQueueView view = Container.Resolve<FileQueueView>(new ParameterOverride("viewModel", vm));

            IRegionManager mainScopeRegionManager = RegionManager.Regions["MainRegion"].Add(view, "FileQueueView", true);

            vm.MainScopeRegionManager = mainScopeRegionManager;
            vm.Initialize();
        }

        private void RegisterTypes()
        {
            Container.RegisterType<FileQueueView>();
            Container.RegisterType<FileQueueViewModel>();
        }
    }
}
