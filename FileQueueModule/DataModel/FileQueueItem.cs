﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonModule.MVVM;
using System.Windows.Media;

namespace FileQueueModule.DataModel
{
    public class FileQueueItem : ObservableItem
    {
        #region Checked

        private bool _checked;
        public bool Checked
        {
            get { return _checked; }
            set
            {
                if (!_checked.Equals(value))
                {
                    _checked = value;
                    NotifyPropertyChanged("Checked");
                }
            }
        }

        #endregion

        #region SourceDirectory

        private String _sourceDirectory;
        public String SourceDirectory
        {
            get { return _sourceDirectory; }
            set
            {
                if (_sourceDirectory == null || !_sourceDirectory.Equals(value))
                {
                    _sourceDirectory = value;
                    NotifyPropertyChanged("SourceDirectory");
                }
            }
        }

        #endregion

        #region StorageObjects

        private List<StorageObject> _storageObjects = new List<StorageObject>();
        public List<StorageObject> StorageObjects
        {
            get { return _storageObjects; }
            set
            {
                if (value != null)
                {
                    _storageObjects.AddRange(value.AsEnumerable());
                }
            }
        }

        #endregion

        #region TargetDirectory

        private String _targetDirectory;
        public String TargetDirectory
        {
            get { return _targetDirectory; }
            set
            {
                if (_targetDirectory == null || !_targetDirectory.Equals(value))
                {
                    _targetDirectory = value;
                    NotifyPropertyChanged("TargetDirectory");
                }
            }
        }

        #endregion
    }
}
