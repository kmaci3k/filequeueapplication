﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using CommonModule.MVVM;

namespace FileQueueModule.DataModel
{
    public class StorageObject : ObservableItem
    {
        #region Color

        private Brush _color;
        public Brush Color
        {
            get { return _color; }
            set
            {
                if (_color == null || !_color.Equals(value))
                {
                    _color = value;
                    NotifyPropertyChanged("Color");
                }
            }
        }

        #endregion

        #region TargetDisk

        private String _targetDisk;
        public String TargetDisk
        {
            get { return _targetDisk; }
            set
            {
                if (_targetDisk == null || !_targetDisk.Equals(value))
                {
                    _targetDisk = value;
                    NotifyPropertyChanged("TargetDisk");
                }
            }
        }

        #endregion
    }
}
