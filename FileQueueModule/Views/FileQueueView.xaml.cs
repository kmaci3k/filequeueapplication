﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FileQueueModule.ViewModels;
using CommonModule.MVVM;

namespace FileQueueModule.Views
{
    /// <summary>
    /// Interaction logic for FileQueueView.xaml
    /// </summary>
    public partial class FileQueueView : UserControl, View<FileQueueViewModel>
    {
        public FileQueueView()
        {
            InitializeComponent();
        }

        public FileQueueView(FileQueueViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }

        private FileQueueViewModel _viewModel;
        public FileQueueViewModel ViewModel 
        {
            get { return _viewModel; }
            private set
            {
                if (_viewModel == null || !_viewModel.Equals(value))
                {
                    DataContext = value;
                    _viewModel = value;
                }
            }
        }
    }
}
