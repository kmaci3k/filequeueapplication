﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Prism.Regions;

namespace CommonModule.MVVM
{
    public abstract class Model : ObservableItem
    {
        [Dependency]
        public IUnityContainer Container { get; private set; }

        [Dependency]
        public IRegionManager RegionManager { get; private set; }

        public Model()
        {

        }

        public virtual void Initialize()
        {

        }
    }
}
